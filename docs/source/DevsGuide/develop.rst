Developing DIRAC and LHCbDIRAC
===============================

.. toctree::
   :maxdepth: 2

Developing the code is not just about editing. You also want to "run" something, usually for testing purposes.
The DIRAC way of developing can be found `here <http://dirac.readthedocs.io/en/latest/DeveloperGuide/index.html>`_
and it applies also to LHCbDIRAC. Please follow carefully especially what's
`there <http://dirac.readthedocs.io/en/latest/DeveloperGuide/DevelopmentEnvironment/DeveloperInstallation/index.html>`_

In general, if you are developing LHCbDIRAC, you should consider that:

  - everything that applies to DIRAC development, also applies to LHCbDIRAC development, so, follow carefully the links above
  - every LHCbDIRAC release has a strong dependency with a DIRAC release. See https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/CONTRIBUTING.md for more info.


Running enhanced unit tests
---------------------------

The bookkeeping system has the option of running unit tests against a real Oracle database.
Assuming you have already followed the instructions to follow the regular unit tests, this can be done by starting a docker container.

.. code-block:: bash

   # This only needs to be done once
   docker login gitlab-registry.cern.ch

   # Start the container from the root of the LHCbDIRAC repository
   docker run --name=testbkdb --rm -d -p 1521:1521 -e ORACLE_SID=bkdbsid -e ORACLE_PDB=bkdbpdb -e ORACLE_PWD=bkdbpass -e ORACLE_CHARACTERSET=AL32UTF8 -v "$PWD:/repo" gitlab-registry.cern.ch/lhcb-dirac/bookkeeping-ci-container/oracle-database-snapshots:19.3.0-ee
   # Watch the logs while the container starts
   docker logs -f testbkdb
   # Once you see, "DATABASE IS READY TO USE!" you can use Control+C to stop following the log
   # Now install the schema
   docker exec -it testbkdb bash -x /repo/tests/Jenkins/bookkeeping_install/setup_ci_bookkeeping_db.sh

Once this is done you can run the tests with:

.. code-block:: bash

   pytest -k 'Test_BookkeepingDB' --oracle-url oracle://system:bkdbpass@localhost:1521/bkdbpdb
