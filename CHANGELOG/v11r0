[v11.0.57]
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Subsystem

CHANGE: (!1705) Add option --Info input to check input files of a transformation against the BK query
For examples look into release.notes

*BKK

FIX: (!1701) raise when running into race condition upon inserting processing



[v11.0.56]
*ProductionRequest

NEW: (!1695) Support APMerge to APSyncAgent
For examples look into release.notes



[v11.0.55]
*DataManagementSystem

NEW: (!1693) StorageUsageDumpAgent also dumps the pandas dataframe

*Core

FIX: (!1691) lazy loading uproot



[v11.0.54]
*Bookkeeping

CHANGE: (!1689) Use files.production in BK stored procedures for better performance

*DataManagementSystem

NEW: (!1685) Add StorageUsageDumpAgent



[v11.0.53]
[v11.0.52]
*AnalysisProductions

NEW: (!1681) Support multiple output filetypes in AP requests

*LegacyOracleBookkeepingDB

CHANGE: (!1681) support `getProductionFilesBulk` query by `[(production ID, filetype), ...]`

*ProductionManagement

CHANGE: (!1679) Don't simplify DataTakingConditions in production tools display

*TransformationSystem

NEW: (!1678) add a APProcessingByFileTypeSize plugin for multi-filetype transformations, which creates tasks grouped by filetype and size for files with replicas on allowed SEs.

*ProductionRequest

FIX : (!1678) support input queries with ProductionID and FileType in transforms
FIX : (!1678) remove `__checkIOTypes` ProductionRequest check to support DAG-type workflows. To be replaced with ProductionRequest model validator (already present on `ProductionBase` model).
FIX : (!1678) provide option (`pr.disableOptionalCorrections`) to turn off spurious ad-hoc corrections to stepsInProds to support DAG-type workflows.
NEW : (!1678) added fields `bkSampleMax` and `bkSampleSeed` for BK query sampling support.

*Bookkeeping

NEW: (!1677) Denormalise files table to include production ID for better performance

*Production

NEW: (!1676) add removal transformations to the dirac-production-management script



[v11.0.51]
*TransformationSystem

FIX: (!1672) Allow Stopped -> Completed transition for transformations

*DataManagementSystem

FIX: (!1671) Fix bulk checking InBKNotInFC

*ProductionManagement

FIX: (!1670) Ignore removed files in ProductionStatusAgent

*ProductionManagementSystem

NEW: (!1669) support multiple input filetypes in local tests.

*Core

FIX: (!1669) fix warnings in BookkeepingJobInfo model and BookkeepingReport module by simply casting to string before assigning fields.



[v11.0.50]
*Bookkeeping

FIX: (!1667) Returning S_ERROR from NewOracleBookkeepingDB
FIX: (!1659) Use new implementation of getFilesWithMetadata

*Production

CHANGE: (!1666) Optimise consistency checks

*NewOracleBookkeepingDB

NEW: (!1662) BK query sampling support for all relevant queries

*TornadoAnalysisProductionsHandler

FIX: (!1661) Only fetch input queries for supported transformation types to fix the APSyncAgent while ensuring consistency in the AnalysisProductionsDB.

Core/Workflow
FIX: (!1658) Introduce "cleanedApplicationName" in ModuleBase and always use it when defining file paths, so they do not break generation of summaryXML or prodConf configs (among other things) for application names e.g. `lb-conda/default`.
Core/Workflow
FIX: (!1658) If the application does not add an output file to the pool catalog, ModuleBase will do it so the next step can resolve the input file paths.
TornadoAnalysisProductionsHandler FIX: Used kwargs for getProductions tornado handler to support older versions of APC and get things working again.
AnalysisProductionsDB FIX: Fixed nasty query bug that only surfaced when querying MySQL (but not SQLite, which is used in tests)
TornadoAnalysisProductionsHandler
FIX: (!1654) Fixed types for getProductions and get/addPublications
AnalysisProductionsClient
FIX: (!1654) Fixed bad calls to RPC methods

*TransformationSystem

FIX: (!1657) Handle more exceptions when loading pickled cache



[v11.0.49]
*ProductionManagementSystem

FIX: (!1649) ensure output of _getBKKQuery in launch_sprucing handles `;;;` in the BK query dictionary
FIX: (!1646) Propagate ExtendedDQOK to the BK Query dictionary

*AnalysisProductionsDB

NEW: (!1648) Add `require_has_publication` boolean flag to getProductions to facilitate querying samples with publications
NEW: (!1648) Add input query to transformation info of getProductions.
CHANGE: (!1648) getPublications now returns samples grouped by publication number, which can be used to view all published samples (or a specific subset).

*BookkeepingSystem

FIX: (!1646) Ensure DataQuality, DataQuality, Quality propagates as DQ flag to BK query dict

AnalysisProductionsDB CHANGE: Allow querying of archived samples. List linked publications with each sample

*ProductionManagement

CHANGE: (!1639) Improvements to dirac-production-manager



[v11.0.48]
AnalysisProductionsDB
CHANGE: (!1634) return samples as long as they are inside the validity period, when `validity_end` is set

*Subsystem

CHANGE: (!1632) speedup TS agent

*TransformationSystem

NEW: (!1630) Support getting bulk input queries for transformations

*ProductionManagement

NEW: (!1628) Add dirac-production-manager
NEW: (!1624) Start to simplify ProductionRequestDB state logic

*BKK

CHANGE: (!1622) reduce the memory footprint of the BookkeepingWatchAgent



[v11.0.47]
*AnalysisProductionsDB

CHANGE: (!1627) add `earliest_housekeeping_due` to query of `listAnalyses2` in AnalysisProductionsDB

*Workflow

FIX: (!1623) UploadMC TypeError exception with datetime objects serialized



[v11.0.46]
*ProductionManagement

FIX: (!1620) Ensure Production name isn't too long
FIX: (!1611) Ensure ProcessingPass is an exact match when searching for a step

*ProductionManagementSystem

NEW: (!1616) Add dirac-ap-run-files command to get the files in an AnalysProduction that contain data from a specific run.

Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Subsystem

NEW/CHANGE/FIX: (!1614) explanation
For examples look into release.notes

*ResourceStatusSystem

FIX: (!1609) Authorise to have several people in the same egroup when there is 3 persons on shift morning/afternoon/night



[v11.0.45]
*CI

NEW: (!1604) use Alma9 instead of CentOS7

*DataManagementSystem

FIX: (!1602) Better error parsing for file not found when checking file integrity



[v11.0.44]


[v11.0.43]
*Core

NEW: (!1593) Support Pydantic 2

*ProductionManagement

FIX: (!1592) Always use root:// in HistogramMergingAgent

*Bookkeeping

FIX: (!1590) SMOG2: return error instead of partial result in case parameters are wrong
FIX: (!1588) Setting ExtendedDQOK to a string when querying for files



[v11.0.42]
*Bookkeeping

FIX: (!1580) Fix the bad refactoring in BKQuery.getLFNs

*ProductionManagement

CHANGE: (!1576) Ignore files in NotProcessed when considering AnalysisProductions in the ProductionStatusAgent
NEW: (!1576) Support setting AnalysisProductions/ForceActiveInput in the CS to override if Analysis Productions are kept open
CHANGE: (!1575) Remove `ProductionRequestHandler.export_getAllSubRequestSummary`

*DataManagementSystem

CHANGE: (!1575) Remove RunDBInterfaceHandler

*ProductionManagementSystem

NEW: (!1558) Add `dirac-ap-input-metadata` command to display information about the inputs to an Analysis Production



[v11.0.41]
*Bookkeeping

NEW: (!1570) Support running dirac-bookkeeping-simulationconditions-insert in batch mode
FIX: (!1567) Support SMOG2 and ExtendedBKOK in DiracLHCb (API)

*Production Management

CHANGE: (!1568) Flush analysis productions even if there are files in MaxReset
CHANGE: (!1568) Treat Analysis Productions transformations with EndDate as not having active input transformations

*TransformationSystem

NEW: (!1568) Support setting EndDate in transformation's bookkeeping query

Please follow the template:
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:
\*DMS
CHANGE: (!1560) Do not favor xroot anymore when transfering between CTA and EOS

*Subsystem

NEW/CHANGE/FIX: (!1565) explanation
For examples look into release.notes

*ProductionManagement

FIX: (!1562) Exclude New transformations from APSyncAgent
FIX: (!1559) Manually specifying run numbers for local tests



[v11.0.40]


[v11.0.39]
[v11.0.38]
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Subsystem

NEW/CHANGE/FIX: (!1554) explanation
For examples look into release.notes

*ProductionManagement

FIX: (!1552) Avoid relying on dictionary order in JSON output of dirac-production-request-submit
NEW: (!1544) Support automatically picking test LFNs in dirac-production-request-run-local



[v11.0.37]
*Bookkeeping

NEW: (!1543) Add BookkeepingClient.getOutputPathsForProdID
CHANGE: (!1543) Modernise NewOracleBookkeepingDB

*dashboards

FIX: (!1540) opensearch plots reported metrics as hourly metrics, while these changes would report them as 15min metrics regardless of bin size.

NEW: (!1537) Bookkeeping: ExtendedDQOK for Runs, with subset of scripts/methods to access (BKQuery).
NEW: (!1534) for Transformations, SMOG2 query accept a single string.

*TransformationSystem

FIX: (!1530) Respect SMOG2 for input BK Queries to transformations



[v11.0.36]
*ProductionRequest

NEW: (!1524) Support submitting productions with SMOG2 requirements

*dashboards

FIX: (!1523) plots in shifter dashboards using opensearch/elasticsearch show metrics hourly metrics regardless of time interval queried.
NEW: (!1523) adding WMS monitoring dashboard



[v11.0.35]
*Workflow

CHANGE: (!1519) Add time stamp field to generatorCounters ES index
CHANGE: (!1519) Enhance processing of GeneratorLog.xml files prior to parsing and extracting JSON for MCStats



[v11.0.34]


[v11.0.33]
*Core/Utilies

CHANGE: (!1509) Enhance processing of GeneratorLog.xml files prior to parsing and extracting JSON for MCStats



[v11.0.32]
\*Bookkeeping
FIX: (!1496) allow Bk query without processing pass, and fix printout of the script

*DataManagementSystem

CHANGE: (!1487) if a specified source is impossible to satisfy in an FTS transfer, use all possible sources



[v11.0.31]
*Bookkeeping

FIX: (!1494) Setting event timeout

*BKK

NEW: (!1480) script to split bookkeeping path by run ranges



[v11.0.30]
*Bookkeeping

FIX: (!1490) Using dirac-bookkeeping-get-run-ranges with heavy ion data

*Workflow

NEW: (!1489) added possibility to specify event timeouts



[v11.0.29]
[v11.0.28]
*TransformationSystem

FIX: (!1479) avoid exception when an ancestor file has no active replica
FIX: (!1479) consider "Payload failed" as a potential IDR error
CHANGE: (!1479) obey a few pylint recommandations



[v11.0.27]
[v11.0.26]
*RequestManagementSystem

NEW: (!1483) Introduce LogSE-EOS-Write for proper TPC



[v11.0.25]


[v11.0.24]
*Production

FIX: (!1476) correct bool logic when submitting Sprucing prod

*DataManagementSystem

CHANGE: (!1473) do not force xroot for Antares to Echo staging

*ProductionManagement

FIX: (!1471) allow runsList along side run start/end

*Bookkeeping

NEW: (!1470) dirac-bookkeeping-get-stats shows also the bytes in binary factors



[v11.0.23]
[v11.0.22]
[v11.0.21]
*AnalysisProductionsDB

NEW: (!1453) Added the function `archiveSamplesAtSpecificTime` that will set the end validity of the given samples to the given datetime. Essentially a minor extension to `archiveSamples`.

*BK

FIX: (!1439) statistic query when the list of run numbers is specified



[v11.0.20]
*ProductionManagement

NEW: (!1442) Support setting ancestorDepth for local production tests
NEW: (!1440) Add test for MC request YAML
NEW: (!1440) Support shorthand for specifying production request filetype visibility
FIX: (!1440) Add more default values in ProductionManagement pydantic Models
NEW: (!1440) Get ProductionBase.author from current proxy if it isn't set already
NEW: (!1440) Make DataProduction.input_dataset.conditions_id optional
NEW: (!1440) Enhance schema for DataProduction.input_dataset.conditions_dict

*TransformationSystem

FIX: (!1441) re-doing the BK query with unchanged timestamp in case of errors



[v11.0.19]
*TransformationSystem

FIX: (!1431) Support using RAWProcessing plugn with Run 3 data

*ProductionsManagement

CHANGE: (!1420) simplified the production YAML



[v11.0.18]
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Subsystem

NEW/CHANGE/FIX: (!1417) explanation
For examples look into release.notes



[v11.0.17]
*ProductionManagement

CHANGE: (!1414) remove SimCondID from ProductionRequest
FIX: (!1413) a slightly more informative PR email

*Bookkeeping

DOC: (!1410) changing sim conditions reference for complete productions

*TransformationSystem

NEW: (!1409) create a Sprucing plugin



[v11.0.16]
*ProductionManagement

CHANGE: (!1406) set keys refPFN and refLFN with default values in HistogramMergingAgent



[v11.0.15]
*TransformationSystem

FIX: (!1404) there may be no message in returned value

*DataManagementSystem

FIX: (!1404) list of site cannot be built dynamically in the default setter



[v11.0.14]
*bkk

CHANGE: (!1402) ignore PLUME in the data taking condition description



[v11.0.13]
*ProductionManagement

NEW: (!1399) Added production submission template for sprucing



[v11.0.12]
[v11.0.11]
*ProductionManagement

FIX: (!1394) speedUp of getProductionRequestSummary RPC call (for MCExtensionAgent)

*WorkloadManagement

CHANGE: (!1393) Remove lb-run specific job rescheduling



[v11.0.10]
\*BK CHANGE: (!1390) increase OPTIONFILES fields length from 1000 to 4000

*ProductionManagement

FIX: (!1387) Support merging steps with dirac-production-request-run-local



[v11.0.9]
*Production

NEW: (!1385) Add --prmon flag to lb-prod-run in RunApplication

*Core

CHANGE: (!1382) a few changes to better integrate RemoteRunner to RunApplication
FIX: (!1375) GangaDataFile only create a filetype mapping dict if we have a persistency



[v11.0.8]
*ResourceStatusSystem

CHANGE: (!1380) export ipv6 info for CE and SE in the topology.json

*ProductionManagement

NEW: (!1377) AnalysisProductions ownership is now handled at the wg+analysis level
NEW: (!1377) Method for making an AnalysisProductions samples from existing requests
NEW: (!1377) New `listAnalyses2` method for AnalysisProductions which provides more information
CHANGE: (!1377) Optimise getProductions
CHANGE: (!1373) Set mcTCK to an empty string when using dirac-production-request-submit



[v11.0.7]
*DataManagementSystem

CHANGE: (!1369) Reduce verbosity of StorageHistoryAgent



[v11.0.6]
*WorkloadManagementSystem

FIX: (!1367) JobDB: removed getTimings method (JobParameters are now in ES)



[v11.0.5]
*DataManagementSystem

FIX: (!1365) fix typo in the print format of the StorageUsageAgent
For examples look into release.notes



[v11.0.4]
*Bookkeeping

FIX: (!1363) Improve performance of JobReader with many output files
NEW: (!1354) Support two implementations of the bookkeeping DB



[v11.0.3]
*DataManagementSystem

FIX: (!1356) fix bad formatting in StorageUsageDB
FIX: (!1349) take into account empty checksum in the DataIntegrityClient

*Production Management

FIX: (!1350) Support sqllchemy 2 in AnalysisProductionsDB
NEW: (!1344) Support using nightly builds with dirac-production-request-run-local

*TransformationSystem

FIX: (!1349) fix import orders in the scripts
CHANGE: (!1343) limit number of files per task for RAWReplication plugin
For examples look into release.notes

*test

CHANGE: (!1349) speedup pylint

*ProductionManagement

FIX: (!1348) Use autostash when pulling upstream SimProdDB changes

*Core

FIX: (!1345) Clean console_scripts that were removed for v11

Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Workflow

FIX: (!1342) UploadLogFile incorrect upload directory



[v11.0.2]
*TransformationSystem

FIX: (!1338) fix bad split for TargetSE and LFNs in RAWReplicationBody

*Bookkeeping

FIX: (!1336) Fix filtering steps by filetype



[v11.0.1]
[v11.0.0]
*TransformationSystem

NEW: (!1334) add WGProcessing plugin



[v11.0.0a14]
*TransformationSystem

FIX: (!1332) Respect fileTypesToKeep in TransformationCleaningAgent

*ProductionManagement

NEW: (!1330) Add num_test_events key to production request YAML
FIX: (!1330) Setting binary_tag when submitting requests using YAML

*DataManagementSystem

CHANGE: (!1328) use root protocol to stage between Antares and Echo

*BKK

FIX: (!1326) specify the method name in the getFilesWithMetadata client call



[v11.0.0a13]
FIX: (!1323) ProductionRequest: when creating the OutputDataFileMask for Testing MC productions, keep all original step output file types

*ProductionManagement

NEW: (!1319) Support setting binary tag when using dirac-production-request-*

*TransformationSystem

FIX: (!1311) Changing import of TS utility (backported to DIRAC)



[v11.0.0a12]
*BKK

FIX: (!1315) use TransferClientSelector to allow for HTTPs BKK

*CI

FIX: (!1315) install LHCbDIRAC server version in container

*ResourceStatusSystem

FIX: (!1313) add ARC6 and AREX to NagiosTopologyAgent



[v11.0.0a11]


[v11.0.0a10]
[v11.0.0a9]


[v11.0.0a8]
*Bookkeeping

NEW: (!1293) added TornadoBookkeepingManager



[v11.0.0a7]
Change/fix: allow Thick mode Oracle connections and specifying configuration directory for aliases
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Bookkeeping

FIX: (!1285) Double S_OK in return value of getFilesWithMetadata
FIX: (!1284) OracleBookkeepingDB.getConditions

*Subsystem

NEW/CHANGE/FIX: (!1281) explanation
For examples look into release.notes



[v11.0.0a6]
*Test

FIX: (!1282) correctly using the new RunApplication interface in Test_RunApplication

*ProductionManagement

FIX: (!1269) from ElasticMCGaussLogErrorsDB to ElasticLogErrorsDB



[v11.0.0a5]
*All

CHANGE: (!1271) use keyword parameter for MySQL calls



[v11.0.0a4]
[v11.0.0a3]
*Tests

NEW: (!1259) add a manual CI job for DMS tests

*Workflow

CHANGE: (!1249) New structure for MC error json file



[v11.0.0a2]
*Core

FIX: (!1220) ProductionData now returns the correct path for log files

*DataManagementSystem

FIX: (!1220) LogUpload failover request can handle both tar and zip files



[v11.0.0a1]
[v11.0.0a0]
*BKK

FIX: (!1218) bkk MC test is a bit less strict about timestamp comparison
